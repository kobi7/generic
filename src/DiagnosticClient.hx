import Sys.*;

// TODO: make sure semantics match variant_mvc. i'm not sure i remember all the mechanism.

class DiagnosticClient<Screens> implements IView<Screens>
{
	public function setupUi():Void {
		println("setting up ui");
	}
	public function requestUpdate():Void {
		println("requesting update");
	}
	public function start():Void {
		println("starting... ");
	}
	public function doExit():Void {
		println("exitting ... ");
	}
	public function startedSendingUpdates():Void {
		println("GOT SIGNAL: started sending updates");	
		println("many updates are coming, change state, but don't show it yet");	
	}
	public function finishedSendingUpdates():Void {
		println("GOT SIGNAL: finished sending updates");		
		println("updates have finished. show the changed state now. (invalidate)");
	}
	
	public function notify(note: String):Void {
		println("in notify");
		println("the note is: "+note);
	}
	
	public function redrawPart(data : SimpleData<Screens>):Void {
		println("redraw part:");
		trace(data);
	}
	public function showErr(resp : Response):Void {
		println("show error:");
		trace(resp);
	}
	public function redrawAll(drawing_data : Map<Screens, SimpleData<Screens>>):Void 
	{		
		println("redraw all. got:");
		trace(drawing_data);
		println("\nrunning redrawPart for each part");
		for (part in drawing_data.keys())
		{
			var data = drawing_data.get(part);
			redrawPart(data);
		}
	}
}