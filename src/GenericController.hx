import Sure.sure as assert;
import Sure.sure as require;
import Sure.sure as ensure;
using Helpers.ArrayTools;

class GenericController<Parts> 
	implements IController<Parts> 
	implements IControllerSpec<Parts> 
{
	public function requestPartialReload(part : Parts) {
		throw "Not implemented";		
	}

	public function customBefore(args : ParsedCommand) {
		throw "Not implemented";		
	}
	public function customAction(args : ParsedCommand) : Response {	
		throw "Not implemented";
	}
	public function customAfter(args : ParsedCommand) {
		throw "Not implemented";
	}
	
	public function load() {
		throw "Not implemented";		
	}
	
	public function getDataForPart(part : Parts) : SimpleData<Parts>{
		throw "Not implemented";		
	} 
	
	public function updateViewPart(v1 : IView<Parts>, data : SimpleData<Parts>):Void
	{
		throw "Not implemented";
	}
	
	
	// todo event reachedLimit : LimitReachedHandler;

	// roles:

	// var _replayer : IRole = new ManageLogReplay();
	// var _historian : IRole = new ManageHistory();
	// var _resumer : IRole = new ManagePersistence();
	
	var _opts = FeatureSet.None;
	public var roles(default,null) : Array<IRole>;
	 
	var views(null,null) : Array<IView<Parts>>;
	
	public function requestPartReload(part : Parts)
	{
		var partData = getDataForPart(part);
		updateViewsPart(partData);
	}
	
	public function makePartsData(): Map<Parts, SimpleData<Parts>>
	{
		throw "notimplemented";
		var dict = new Map<Parts,SimpleData<Parts>>();
		var allParts:Array<SimpleData<Parts>> = [ "screen1", "screen2" ]; //TODO: figure out or change		 representation to a set. 
		
		for (enumEntry in allParts)
		{	
			var data = getDataForPart(enumEntry);
			var simple = new SimpleData<Parts>(enumEntry,data);
			dict.set(enumEntry,simple);
		}
		return dict;
	}
	public function updateViews() 
	{ 
		var dict = makePartsData();
		for (k in dict.keys())
		{
			var val = dict.get(k);
			updateViewsPart(val); 
		}
	}
	public function updateViewsPart(data : SimpleData<Parts>)
	{
		for (v in views) v.redrawPart(data);
	}
		
	public function requestReload()
	{
		updateViews();
	}
	
	public function new(view : IView<Parts>, opts : FeatureSet = FeatureSet.None)
	{
		_opts = opts;
	 	attachView(view);
		if (FeatureSet.contains(_opts, FeatureSet.Profile))
		{
			// todo events. listen reachedLimit, ref doSaveState
		}
			
		// _roles =  [ _replayer, _historian, _resumer ];
	}
	public function doSaveState() {
		throw ("not implemented");
	}
		
	
	public function attachView(v1 : IView<Parts>) {
		if (!views.contains(v1))
			views.push(v1);
			
	}

	public function detachView(v1 : IView<Parts>)
		if (views.contains(v1)) views.remove(v1);
		
	public function performAction(cmd : String) : Response
	{
		assert( ParsingUtils.isValidCommand(cmd));
	
		// ctrl = this to IControllerSpec
		var parsed = ParsingUtils.parseCommand(cmd);
		beforeAction(parsed); // # generic controller
		var res = customAction(parsed); //# custom controller
		afterAction(parsed); // # generic controller
		return res;
	}
		
	public function notifyViews(note : String){
		for (v in views) v.notify(note);}
	
	public function sendErrToViews(err : Response){
		for (v in views) v.showErr(err);}
	


	public function beforeAction(args : ParsedCommand)
	{	
		/*
		// log(args.toString)
		if Options.Profile in _opts
		 recordProfile(args)
		*/
		
		customBefore(args);
	}
		
	public function afterAction(args : ParsedCommand) 
	{
	/*	if Options.Profile in _opts
		recordProfile(args)
		*/	
	//finally
		customAfter(args);
	}
		
	
}